import os
import time

import requests
from flask import request, Flask, render_template
from xml.etree import ElementTree as ET
import urllib.parse as urlparse
from urllib.parse import parse_qs
import json 
import re


application = Flask(__name__)
app = application

source = "https://raw.githubusercontent.com/devdattakulkarni/elements-of-web-programming/master/data/austin-pool-timings.xml"

data = requests.get(source)

root = ET.fromstring(data.text)


@app.route("/pools")
def get_pools():

  all_pools = []
  for pool in root.findall("row"):
  	my_dict_pools = dict()
  	pool_name = pool.find("pool_name").text
  	pool_type = pool.find("pool_type").text
  	pool_status = pool.find("status").text
  	pool_open_dates = pool.find("open_date").text
  	pool_weekend = pool.find("weekend").text
  	pool_weekday = pool.find("weekday").text

  	my_dict_pools["Name"] = pool_name
  	my_dict_pools["Type"] = pool_type
  	my_dict_pools["Status"] = pool_status
  	my_dict_pools["Open_date"] = pool_open_dates
  	my_dict_pools["Weekend"] = pool_weekend
  	my_dict_pools["Weekday"] = pool_weekday

  	all_pools.append(my_dict_pools)

  return json.dumps(all_pools)


@app.route("/")
def pool_info_website():
	return render_template('index.html')

if __name__ == "__main__":
	app.debug = True
	app.run(host='0.0.0.0')
